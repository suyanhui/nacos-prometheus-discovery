package model

type Config struct {
	LogLevel         string       `json:logLevel`         // info
	NacosHost        string       `json:nacosHost`        // "http://test1:8684/nacos"
	IntervalInSecond int          `json:intervalInSecond` // 60
	NamespaceId      string       `json:namespaceId`      // "prod"
	Group            string       `json:group`            // "DEFAULT_GROUP"
	Cluster          string       `json:cluster`          // "DEFAULT"
	ConfigList       []ConfigItem `json:configList`       //
}

type ConfigItem struct {
	TargetFilePath string `json:targetFilePath` // "target/gen-target.json"
	DataId         string `json:dataId`         // "application-prod.yml"

}
